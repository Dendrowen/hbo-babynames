package com.dendrowen;

import com.dendrowen.util.MapUtil;

import java.io.Serializable;
import java.util.*;

public class Name implements Comparable<Name>, Serializable {

    public static final int NAME = 1;
    public static final int SEX = 3;
    public static final int NUMBER = 5;

    private static final Object lock = new Object();
    
    public static volatile LinkedHashMap<Name, Integer> names = new LinkedHashMap<>();

    public static synchronized void addAll(LinkedHashMap<Name, Integer> submittedNames){
        submittedNames.forEach(
                (k, v) -> names.merge(k, v, (v1, v2) -> v1 + v2)
        );

    }

    public static void addName(String row){
        String[] exp = row.split(",");
        Name name = new Name(exp[NAME], exp[SEX].charAt(0));

        if(!names.containsKey(name)){
            addNameSync(name, Integer.parseInt(exp[NUMBER]));
        }
        else{
            synchronized (lock) {
                names.put(name, names.get(name) + Integer.parseInt(exp[NUMBER]));
            }
        }
    }

    private static synchronized void addNameSync(Name name, int n){
        if(!names.containsKey(name)) names.put(name, n);
    }

    public static synchronized void printTop(){
        //sortNames();
        names = MapUtil.sortByValue(names);

        LinkedHashMap<Name, Integer> topM = new LinkedHashMap<>();
        LinkedHashMap<Name, Integer> topF = new LinkedHashMap<>();

        for(Map.Entry<Name, Integer> name : names.entrySet()){
            if(name.getKey().sex == 'M' && topM.size() < 6){
                topM.put(name.getKey(), name.getValue());
            }
            if(name.getKey().sex == 'F' && topF.size() < 6){
                topF.put(name.getKey(), name.getValue());
            }
            if(topM.size() == 5 && topF.size() == 5){
                break;
            }
        }

        System.out.println("============= MALES ===========");
        for(Map.Entry<Name, Integer> name : topM.entrySet()) System.out.printf("%20s [%,d]\n", name.getKey(), name.getValue());
        System.out.println("============ FEMALES ==========");
        for(Map.Entry<Name, Integer> name : topF.entrySet()) System.out.printf("%20s [%,d]\n", name.getKey(), name.getValue());
    }

//    public static void sortNames(){
//
//        HashMap<Name, Integer> sorted = new HashMap<>();
//        while(names.size() > 0) {
//            int max = 0;
//            Name maxKey = null;
//            for (Map.Entry<Name, Integer> name : names.entrySet()) {
//                if (name.getValue() > max) {
//                    maxKey = name.getKey();
//                    max = name.getValue();
//                }
//            }
//            sorted.put(maxKey, max);
//            names.remove(maxKey);
//        }
//        names = sorted;
//    }



    private final String name;
    private final char sex;


    public Name(String name, char sex){
        this.name = name;
        this.sex = sex;
    }

    public char getSex() {
        return sex;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Name o) {
        if(this.name.compareTo(o.name) != 0)
            return this.name.compareTo(o.name);

        if(this.sex != o.sex)
            return this.sex - o.sex;

        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Name)) return false;
        return compareTo((Name) o) == 0;
    }

    @Override
    public int hashCode(){
        return (name + "." + sex).hashCode();
    }

    @Override
    public String toString() {
        return String.format("[%s] %15s", sex, name);
    }
}
