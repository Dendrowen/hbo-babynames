package com.dendrowen;

import java.io.*;

public class LoaderFactory {

    private Loader next = null;
    private Thread nextGenerator = new Thread(this::createNext);
    private BufferedReader br;
    private int partitionSize;
    private boolean eof = false;


    public LoaderFactory(String filename, int partitionSize) throws FileNotFoundException {
        this.partitionSize = partitionSize;
        File file = new File("../../../assets/" + filename);
        br = new BufferedReader(new FileReader(file));
        nextGenerator.start();
    }

    public synchronized Loader getNext() throws InterruptedException, EOFException {
        nextGenerator.join();
        if(next == null) throw new EOFException("End of file");
        Loader l = next;
        next = null;
        nextGenerator = new Thread(this::createNext);
        nextGenerator.start();
        return l;
    }

    private void createNext(){
        if(eof) return;
        StringBuilder str = new StringBuilder();
        String line = "";
        for(int i = 0; i < partitionSize; i++){
            try {
                line = br.readLine();
                if(line == null) throw new EOFException("end of file");
                str.append(line).append("\n");
            } catch (IOException e) {
                eof = true;
                break;
            }
        }
        next = Loader.loaderFromString(str.toString());
    }


}
