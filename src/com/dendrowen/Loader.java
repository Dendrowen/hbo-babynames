package com.dendrowen;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class Loader implements Runnable, Serializable {

    private BufferedReader br;
    private int count = 0;
    private int length = -1;

    /**
     * Run this class to test it all. Usually you wouldn't process the data this way.
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("started\n===============================================================================");

        long serialTime = loadSerial();
        System.out.printf("                       Serial took %6.3f seconds\n", serialTime / 1000.0);
        //Name.printTop();
        Name.names.clear();
        long threadLockTime = loadThreadLock();
        System.out.printf("                   Threadlock took %6.3f seconds, that's %d%%\n", threadLockTime / 1000.0, 100 * threadLockTime / serialTime);
        //Name.printTop();
        Name.names.clear();
        long threadPoolTime = loadThreadPool();
        System.out.printf("                   Threadpool took %6.3f seconds, that's %d%%\n", threadPoolTime / 1000.0, 100 * threadPoolTime / serialTime);
        //Name.printTop();

        System.exit(0);
    }

    /**
     * Load all the data sequentially from a single file
     * @return the time the method took in ms
     */
    public static long loadSerial() {
        long start = System.currentTimeMillis();
        try {
            Loader l = loaderFromFilename("StateNames.csv");
            l.run();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    /**
     * Read all the data from the files parallel through threads and locks
     * @return the time the method took in ms
     */
    public static long loadThreadLock() {
        long start = System.currentTimeMillis();
        try {
            LoaderFactory lf = new LoaderFactory("StateNames.csv", 250000);
            List<Thread> threads = new ArrayList<>();
            while(true) {
                try {
                    Thread t = new Thread(lf.getNext());
                    t.start();
                    threads.add(t);
                } catch (EOFException e){
                    break;
                }
            }
            for (Thread t : threads) t.join();
        } catch (FileNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    /**
     * Refined the above method with a pool
     * @return the time the method took in ms
     */
    public static long loadThreadPool(){
        long start = System.currentTimeMillis();

        ThreadPoolExecutor executor =
                (ThreadPoolExecutor)
                        Executors.newFixedThreadPool(8);
        Collection<Future<?>> futures = new LinkedList<>();
        LoaderFactory lf = null;
        try {
            lf = new LoaderFactory("StateNames.csv", 250000);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while(true){
            try{
                futures.add(
                    executor.submit(
                        new Thread(
                                lf.getNext()
                        )
                    )
                );
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (EOFException e) {
                break;
            }
        }
        try {
            for (Future<?> future:futures) {
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis() - start;
    }

    public static Loader loaderFromFilename(String filename) throws FileNotFoundException {
        File file = new File("../../../assets/" + filename);
        return new Loader(new BufferedReader(new FileReader(file)));
    }

    public static Loader loaderFromString(String content) {
        Reader input = new StringReader(content);
        return new Loader(new BufferedReader(input));
    }

    /**
     * Constructor. By now I think I should've split this class up into multiple responsibilities.
     * @param br
     */
    private Loader(BufferedReader br) {
        this.br = br;
    }

    @Override
    public void run() {
        String st;
        try {
            while ((st = br.readLine()) != null && (length == -1 || length > count)) {
                //System.out.println(st);
                count++;
                Name.addName(st);
            }
        }catch (IOException e) {
            System.out.println("failed");
        }
    }
}
