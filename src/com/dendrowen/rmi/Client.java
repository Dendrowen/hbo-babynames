package com.dendrowen.rmi;

import com.dendrowen.Loader;
import com.dendrowen.Name;

import java.io.EOFException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * This class acts more like a facade. For actual client functionality, see Loader.java
 */

public class Client {

    public static final String SERVER_IP = "192.168.192.30";
    private static RemoteLoader stub;

    /**
     * Constructor made private
     */
    private Client(){}

    public static void main(String[] args) {
        while(true){
            try{
                Loader.loaderFromString(Client.load()).run();
                submit(Name.names);
                Name.names.clear();
            } catch (Exception e){
                break;
            }
        }
    }

    /**
     * Load the client, register with the server and request some data to process
     * @return the requested date from the server
     * @throws EOFException
     */
    public static String load() throws EOFException {
        try {
            stub = (RemoteLoader) Naming.lookup("rmi://" + SERVER_IP + ":1099/babynames");
            return stub.requestData();
        } catch (RemoteException | NotBoundException | MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Must be called after the load() method when the data is processed
     */
    public static void submit(LinkedHashMap<Name, Integer> names) throws Exception {
        stub.submit(names);
    }

}
