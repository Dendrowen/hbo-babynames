package com.dendrowen.rmi;

import com.dendrowen.Name;

import java.io.*;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Server extends UnicastRemoteObject implements RemoteLoader, Serializable {

    public static final String FILENAME = "StateNames.csv";
    public static final int LINES_PER_REQUEST = 250000;

    private static volatile int counter = 0;
    private static volatile int runningTasks = 0;
    private static volatile boolean outOfData = false;
    private static BufferedReader data;
    private static volatile long start = 0;

    /**
     * Constructor made private
     * @throws RemoteException
     */
    private Server() throws RemoteException, FileNotFoundException {
        System.out.println("working dir: " + System.getProperty("user.dir"));
        data = new BufferedReader(new FileReader("../../../assets/" + FILENAME));
    }

    /**
     * For starting the server when using external clients
     * @param args
     */
    public static void main(String[] args) {
        start();
    }

    /**
     * Request data from the server. When there is no more data, an EOFException is thrown
     * @return The requested data
     * @throws RemoteException
     * @throws EOFException
     */
    @Override
    public synchronized String requestData() throws EOFException {
        if(start == 0) start = System.currentTimeMillis();
        StringBuilder content = new StringBuilder();
        try {
            String line;
            for(int i = 0; i < LINES_PER_REQUEST; i++){
                line = data.readLine();
                if(line == null) throw new IOException();
                content.append(line);
                content.append("\n");
            }
            try {
                System.out.println(getClientHost() + ": requested line " + (counter * LINES_PER_REQUEST + 1) + " - " + ++counter * LINES_PER_REQUEST);
            } catch (ServerNotActiveException e) {
                e.printStackTrace();
            }
        }
        catch (IOException e) {
            handleEnding();
            if(content.length() < 5) {
                throw new EOFException("No more data");
            }
        }
        runningTasks++;
        return content.toString();
    }

    public synchronized void handleEnding(){
        if(runningTasks <= 0 && !outOfData){
            outOfData = true;
            System.out.printf("          RMI took %6.3f seconds\n", (System.currentTimeMillis() - start) / 1000.0);
            new Thread(() -> {
                Name.printTop();
                try {
                    data.close();
                } catch (IOException e1) {
                    System.out.println("Closing file failed");
                }
                System.exit(-1);
            }).start();
        }
    }

    /**
     * Must be called after the requested data is processed
     * @throws RemoteException
     */
    public void submit(LinkedHashMap<Name, Integer> submittedNames){

        Name.addAll(submittedNames);

        runningTasks--;
        try {
            System.out.println(getClientHost() + ": done, running tasks: " + runningTasks);
        } catch (ServerNotActiveException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the server and bind is to the registry
     */
    public static void start() {
        try {
            System.setProperty("java.rmi.server.hostname", Client.SERVER_IP);
            LocateRegistry.createRegistry(1099);
            Naming.rebind("rmi://localhost:1099/babynames", new Server());
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
