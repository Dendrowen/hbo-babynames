package com.dendrowen.rmi;

import com.dendrowen.Name;

import java.io.EOFException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedHashMap;

public interface RemoteLoader extends Remote {

    String requestData() throws RemoteException, EOFException;
    void submit(LinkedHashMap<Name, Integer> submittedNames) throws RemoteException;

}
