package com.dendrowen.util;

import com.dendrowen.Name;

import java.util.*;

public class MapUtil {
    public static synchronized LinkedHashMap<Name, Integer> sortByValue(LinkedHashMap<Name, Integer> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<Name, Integer> > list =
                new LinkedList<Map.Entry<Name, Integer> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, (o1, o2) -> -(o1.getValue()).compareTo(o2.getValue()));

        // put data from sorted list to hashmap
        LinkedHashMap<Name, Integer> temp = new LinkedHashMap<Name, Integer>();
        for (Map.Entry<Name, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
}